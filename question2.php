<!DOCTYPE html>
<html>
<body>

<?php
function analyzeMsg($input,$output){

	$array_input = str_split($input);
	$array_output = str_split($output);
	
	$pattern = "";
	$compare = "";
	$error = false;
	
	foreach($array_input as $key => $value){
		if(!$error){
			
			if($compare == ""){
				$compare = $value + $array_output[$key];
			}else{
				$compare = $total;
			}
			
			$total = $value + $array_output[$key];
			
			if($compare == $total){
				$pattern = "summing up to ".$total;
			}else{
				$error = true;
				$pattern = "";	
			}
		}
	}
	
	if($error){
		foreach($array_input as $key => $value){
	
			if($compare == ""){
				$compare = $value + $array_output[$key];
			}else{
				$compare = $total;
			}
			
			$total = $value - $array_output[$key];
			
			if($compare == $total){
				if($total > 0){
					$pattern = "adding ".($total * -1);
				}else{
					$pattern = "adding ".abs($total);
				}
			}else{
				$pattern = "";	
			}
		}
	}
	
	if($pattern != ""){
		echo "Mike encrypts his message by ".$pattern." to each original character.";
	}else{
		echo "No pattern.";
	}
    
}

analyzeMsg("31625","64958");
?>

</body>
</html>
