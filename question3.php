<!DOCTYPE html>
<html>
<body>

<?php
function testSummary($array){
	
	$student = "";
	$gender = "";
	$math_score = 0;
	$science_score = 0;
	$subject = array();
	$subject_title = "";
	
	foreach($array as $key => $value){
		
		$subject = array();
		$subject_title = "";
			
		$student = $value['Name'];
		
		if($value['Gender'] == "Female"){
			$gender = "she";
		}else{
			$gender = "he";
		}
		
		$math_score = $value['Mathematics'];
		$science_score = $value['Science'];
		
		$average = ($math_score + $science_score) / 2;
		
		if($math_score < 50){
			$subject[] = "Mathematics";
		}
		
		if($science_score < 50){
			$subject[] = "Science";
		}
		
		if(sizeof($subject) > 0){
			$subject_title = implode(" and ",$subject);
		}
		
		if($average >= 50){
			echo $student." has an average score of ".$average." from this test. Overall, ".$gender." is performing very well in this test.<br />";
		}else{
			echo $student." has an average score of ".$average." from this test. However, ".$gender." is not doing well for the ".$subject_title." subject.<br />";
		}
	
	}
	
}

$array = array(0 => array('Name'=>'Annie', 'Gender'=>'Female', 'Mathematics'=>70, 'Science'=>50), 1 => array('Name'=>'Max', 'Gender'=>'Male', 'Mathematics'=>20, 'Science'=>70), 2 => array('Name'=>'Tom','Gender'=>'Male', 'Mathematics'=>40, 'Science'=>30));
testSummary($array);
?>

</body>
</html>
