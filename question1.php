<?php
// Start the session
session_start();
?>
<!DOCTYPE html>
<html>
<body>

<?php
function checkDownload($memberType, $fileType){

	$current_datetime = date("Y-m-d H:i:s");

	$diff = 0;
	
	if($memberType == "member"){
		
		$current = strtotime($current_datetime);
		
		if($_SESSION['nonmember_download_time']){
			$session = strtotime($_SESSION['member_download_time']);
			$diff = $current - $session;
		}
    
    	if(!isset($_SESSION['member_download_count']) || $_SESSION['member_download_count'] < 2){
        
        	echo "Your download ".$fileType." is starting...";
            
            $_SESSION['member_download_count'] += 1;
            $_SESSION['member_download_time'] = date("Y-m-d H:i:s");
            
        }elseif($_SESSION['member_download_count'] >= 2 && $diff < 5){
        
        	echo "Too many downloads";
            
        }elseif($_SESSION['member_download_count'] >= 2 && $diff >= 5){
        
        	echo "Your download ".$fileType." is starting...";
            
            $_SESSION['member_download_count'] = 1;
			$_SESSION['member_download_time'] = date("Y-m-d H:i:s");
            
        }
    	
        
    }elseif($memberType == "nonmember"){
    
		$current = strtotime($current_datetime);
		
		if($_SESSION['nonmember_download_time']){
			$session = strtotime($_SESSION['nonmember_download_time']);
			$diff = $current - $session;
		}
				
    	if(!isset($_SESSION['nonmember_download_count']) || $_SESSION['nonmember_download_count'] < 1){
        
        	echo "Your download ".$fileType." is starting...";
            
            $_SESSION['nonmember_download_count'] += 1;
            $_SESSION['nonmember_download_time'] = date("Y-m-d H:i:s");
            
        }elseif($_SESSION['nonmember_download_count'] >= 1 && $diff < 5){
        
        	echo "Too many downloads";
            
        }elseif($_SESSION['nonmember_download_count'] >= 1 && $diff >= 5){
        
        	echo "Your download ".$fileType." is starting...";
            
            $_SESSION['nonmember_download_count'] = 1;
			$_SESSION['nonmember_download_time'] = date("Y-m-d H:i:s");
            
        }
    	
    }
    
}

checkDownload("nonmember","jpeg");
?>

</body>
</html>
