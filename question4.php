<?php
session_start();
?>
<!DOCTYPE html>
<html>
<body>

<?php
	
	$wallet = 1000;
	$bank = 20.20;
		
	if(isset($_POST['action']) && $_POST['action'] == "Deposit"){
		
		$wallet_balance = $wallet;
		
		if(isset($_SESSION['withdraw'])){
			$wallet_balance += $_SESSION['withdraw'];
		}
		
		if(isset($_SESSION['deposit'])){
			$wallet_balance -= $_SESSION['deposit'];
		}
		
		if($_POST['amount'] < $wallet_balance){
			
			if(isset($_SESSION['deposit'])){
				$_SESSION['deposit'] += $_POST['amount'];
			}else{
				$_SESSION['deposit'] = $_POST['amount'];
			}
			
			$bonus = $_SESSION['deposit'] / 10;
			
			if($bonus < 50){
				
				if(isset($_SESSION['bonus'])){
					$total = $bonus + $_SESSION['bonus'];
				}else{
					$total = $bonus;
				}
				
				if($total < 50){
					if(isset($_SESSION['bonus'])){
						$_SESSION['bonus'] += $bonus;
					}else{
						$_SESSION['bonus'] = $bonus;
					}
				}else{
					$_SESSION['bonus'] = 50;
				}
				
			}else{
				
				$_SESSION['bonus'] = 50;
				
			}
			
		}else{
			echo "Insufficient wallet balance. Action denied.";
		}
		
	}elseif(isset($_POST['action']) && $_POST['action'] == "Withdraw"){
		
		$bank_balance = $bank;
		
		if(isset($_SESSION['withdraw'])){
			$bank_balance -= $_SESSION['withdraw'];
		}
		
		if(isset($_SESSION['deposit'])){
			$bank_balance += $_SESSION['deposit'];
		}
		
		if(isset($_SESSION['bonus'])){
			$bank_balance += $_SESSION['bonus'];
		}
		
		if($_POST['amount'] < $bank_balance){
			
			$_SESSION['withdraw'] = $_POST['amount'];
			
			$_SESSION['withdraw_count'] = 1;
			
		}else{
			
			echo "Insufficient bank credits. Action denied.";
			
		}
	}
	
	if(isset($_SESSION['withdraw']) || isset($_SESSION['deposit']) || isset($_SESSION['bonus'])){
		
		if(isset($_SESSION['withdraw'])){
			$wallet += $_SESSION['withdraw'];
			$bank -= $_SESSION['withdraw'];
		}
		
		if(isset($_SESSION['deposit'])){
			$wallet -= $_SESSION['deposit'];
			$bank += $_SESSION['deposit'];
		}
		
		if(isset($_SESSION['bonus'])){
			$bank += $_SESSION['bonus'];
		}
		
	}
?>
<div>Wallet Balance: $<?php echo $wallet;?></div>
<div>Bank Account Balance: $<?php echo number_format($bank,2);?></div>

<form name="form" method="post" onSubmit="return validate()">
<div style="margin-top: 20px;">
$ <input type="number" name="amount">
</div>

<div style="margin-top: 10px;">
<input type='submit' name="action" value='Deposit'>
<?php if(!isset($_SESSION['withdraw_count'])){?>
<input type='submit' name="action" value='Withdraw'>
<?php }?>
</div>
</form>

<script type="text/javascript">
function validate(){
  let x = document.forms["form"]["amount"].value;
  if (x == "") {
	alert("Amount must be filled out");
	return false;
  }else if (x < 200) {
	alert("Amount must be greater than 200");
	return false;
  }
}
</script>

</body>
</html>
